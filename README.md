 
## COMPILATION
```
mkdir build
cd build
{setup AnalysisBase here, for example "asetup AnalysisBase,21.2.109,here"}
cmake ../source
make
source x86_64-*/setup.sh
```

After doing this, you can setup in following log-ins with

```
cd build
asetup --restore
source x86_64-*/setup.sh
cd ..
```

# EXECUTION
```
mkdir run
cd run
../source/ATestRun.py -d <PATH_TO_TRUTH_ROOT_FOLDER> --submission-dir=<NAME_OF_OUTPUT_FOLDER>
```

# TYPICAL PROBLEMS

One typical issue could be to try to retrieve a container not present in the TRUTH derivation used as input (i.e. TruthHardParticles)

To know the containers available in a TRUTH file you can run:

```
checkxAOD.py {DAOD_TRUTH**root}
```



