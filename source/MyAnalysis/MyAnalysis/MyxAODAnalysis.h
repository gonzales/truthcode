#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H


#include <SampleHandler/SampleHandler.h>
#include <SampleHandler/MetaObject.h>
#include <SampleHandler/MetaFields.h>
#include <EventLoop/Worker.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>

#include <TTree.h>
#include <TH1D.h>
#include <TFile.h>
#include <TGraph.h>
#include <vector>
#include "CxxUtils/fpcompare.h"
#include "xAODBase/IParticle.h"

#include <xAODEventInfo/EventInfo.h>
#include <xAODTruth/TruthEventContainer.h>
#include <xAODTruth/TruthEvent.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODTruth/TruthVertex.h>
#include <xAODTruth/TruthVertexContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMissingET/MissingET.h>

#include "LHAPDF/LHAPDF.h"
#include "LHAPDF/Reweighting.h"

#include "PMGTools/PMGTruthWeightTool.h"

#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "xAODRootAccess/TEvent.h"

#include <MyAnalysis/BookKeeper.hh>


using namespace std;

namespace xAOD {
   class IParticle;
}

class MyxAODAnalysis : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~MyxAODAnalysis();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  void printEvent(const xAOD::TruthEvent* event);
  void printVertex(const xAOD::TruthVertex* vertex);
  void printParticle(const xAOD::TruthParticle* particle);


  

private:

  TString sample_name;

  int processed_events = 0;
  int maxEvents = -1;
  unsigned int m_DAOD_type;
  bool print_event;
  bool doPDFUncertainty = false;
  int run_number = 0;
  string dir_tail = "";
  double sumOfWeights;
  Counts counts;

  vector<Double_t> weighted_yields [13];
  vector<Double_t> weighted_events;

  const LHAPDF::PDFSet * pdf_set;
  std::vector<LHAPDF::PDF*> pdf_sets;
  string submission_dir = "";
  string PDFSet = "CT10";

  asg::AnaToolHandle<PMGTools::IPMGTruthWeightTool> m_weightTool;

  Double_t xSection;
  Double_t filterEff;

  unsigned int m_runNumber = 0;
  unsigned long long m_eventNumber = 0;
  vector<float> *m_jetPt = nullptr;
  vector<float> *m_jetPtx = nullptr;
  vector<float> *m_jetPty = nullptr;
  vector<float> *m_jetPtz = nullptr;
  vector<float> *m_jetEta = nullptr;
  vector<float> *m_jetPhi = nullptr;
  vector<float> *m_jetE = nullptr;
  vector<float> *m_axPt = nullptr;
  vector<float> *m_axPtx = nullptr;
  vector<float> *m_axPty = nullptr;
  vector<float> *m_axPtz = nullptr;
  vector<float> *m_axEta = nullptr;
  vector<float> *m_axPhi = nullptr;
  vector<float> *m_photonPt = nullptr;
  vector<float> *m_photonEta = nullptr;
  vector<float> *m_photonPhi = nullptr;
  vector<float> *m_photonZVtx = nullptr;
  vector<float> *m_vax_x = nullptr;
  vector<float> *m_vax_y = nullptr;
  vector<float> *m_vax_z = nullptr;
  vector<float> *m_axE = nullptr;
  vector<float> *m_axStatus = nullptr;
  vector<float> *met_mpx = nullptr;
  vector<float> *met_mpy = nullptr;
  vector<float> *met = nullptr;
  vector<float> *met_phi = nullptr;
  vector<float> *sumet = nullptr;
  vector<float> * sigmaEff_weighted = nullptr;
  float met_wAxion = 0;
  float met_wAxion_mpx = 0;
  float met_wAxion_mpy = 0;
  float met_wAxion_phi = 0;
  float HT = 0;
  float sHT = 0;
  float HT_mpx = 0;
  float HT_mpy = 0;
  float HT_phi = 0;
  double deltaR_jet_ax = 0;
  double deltaR_jet_met = 0;
  int n_electrons = 0;
  int n_muons = 0;
  int n_photons = 0;
  int n_jet = 0;
  int n_ax = 0;
  int n_vertex = 0;
  float s = 0;
  float deltaPhi_jet_met = 0;
  float deltaPhi_dijet_met = 0;
  float dPhiDiCentralJets = 0;
  float dRDijet = 0;
  float DijetMass = 0;
  float deltaPhi_HT_met = 0;
  vector<int> *n_outgoing = nullptr;
  float hv_x = 0;
  float hv_y = 0;
  float hv_z = 0;
  float mT_boson_decay = 0;
  float mT_H_decay = 0;
  float mcweight;
  float weight;
  vector<Double_t> * pdf_weights = nullptr;

  int yield = 0;
};

MyxAODAnalysis::~MyxAODAnalysis() {
   if (m_jetEta) delete m_jetEta;
   if (m_jetPhi) delete m_jetPhi;
   if (m_jetPt)  delete m_jetPt;
   if (m_jetPtx)  delete m_jetPtx;
   if (m_jetPty)  delete m_jetPty;
   if (m_jetPtz)  delete m_jetPtz;
   if (m_jetE)   delete m_jetE;
   if (m_axEta) delete m_axEta;
   if (m_axPhi) delete m_axPhi;
   if (m_axPt)  delete m_axPt;
   if (m_axPtx)  delete m_axPtx;
   if (m_axPty)  delete m_axPty;
   if (m_axPtz)  delete m_axPtz;
   if (m_photonPt) delete m_photonPt;
   if (m_photonEta) delete m_photonEta;
   if (m_photonPhi) delete m_photonPhi;
   if (m_vax_x)  delete m_vax_x;
   if (m_vax_y)  delete m_vax_y;
   if (m_vax_z)  delete m_vax_z;
   if (m_axE)   delete m_axE;
   if (m_axStatus) delete m_axStatus;
}

void MyxAODAnalysis::printEvent(const xAOD::TruthEvent* event) {
    cout << "--------------------------------------------------------------------------------\n";
    cout << "GenEvent: #" << "NNN" << "\n";
    cout << " Entries this event: " << event->nTruthVertices() << " vertices, " << event->nTruthParticles() << " particles.\n";
    cout << "                                    GenParticle Legend\n";
    cout << "        Barcode   PDG ID      ( Px,       Py,       Pz,     E ) Stat  DecayVtx\n";
    cout << "--------------------------------------------------------------------------------\n";
    for (unsigned int iv = 0; iv < event->nTruthVertices(); ++iv) {
      if (event->truthVertex(iv) != 0) printVertex(event->truthVertex(iv));
    }
    cout << "--------------------------------------------------------------------------------\n";
  }

  void MyxAODAnalysis::printVertex(const xAOD::TruthVertex* vertex) {
   std::ios::fmtflags f( cout.flags() ); 
    cout << "TruthVertex:";
    if (vertex->barcode() != 0) {
      if (vertex->x() != 0.0 && vertex->y() != 0.0 && vertex->z() != 0.0) {
        cout.width(9);
        cout << vertex->barcode();
        cout << " ID:";
        cout.width(5);
        cout << vertex->id();
        cout << " (X,cT)=";
        cout.width(9);
        cout.precision(2);
        cout.setf(ios::scientific, ios::floatfield);
        cout.setf(ios_base::showpos);
        cout << vertex->x() << ",";
        cout.width(9);
        cout.precision(2);
        cout << vertex->y() << ",";
        cout.width(9);
        cout.precision(2);
        cout << vertex->z() << ",";
        cout.width(9);
        cout.precision(2);
        cout << vertex->t();
        cout.setf(ios::fmtflags(0), ios::floatfield);
        cout.unsetf(ios_base::showpos);
        cout << endl;
      } else {
        cout.width(9);
        cout << vertex->barcode();
        cout << " ID:";
        cout.width(5);
        cout << vertex->id();
        cout << " (X,cT): 0";
        cout << endl;
      }
    } else {
      if (vertex->x() != 0.0 && vertex->y() != 0.0 && vertex->z() != 0.0) {
        cout.width(9);
        cout << (void*)vertex;
        cout << " ID:";
        cout.width(5);
        cout << vertex->id();
        cout << " (X,cT)=";
        cout.width(9);
        cout.precision(2);
        cout.setf(ios::scientific, ios::floatfield);
        cout.setf(ios_base::showpos);
        cout << vertex->x();
        cout.width(9);
        cout.precision(2);
        cout << vertex->y();
        cout.width(9);
        cout.precision(2);
        cout << vertex->z();
        cout.width(9);
        cout.precision(2);
        cout << vertex->t();
        cout.setf(ios::fmtflags(0), ios::floatfield);
        cout.unsetf(ios_base::showpos);
        cout << endl;
      } else {
        cout.width(9);
        cout << (void*)vertex;
        cout << " ID:";
        cout.width(5);
        cout << vertex->id();
        cout << " (X,cT):0";
        cout << endl;
      }
    }
    for (unsigned int iPIn = 0; iPIn<vertex->nIncomingParticles(); ++iPIn) {
      if ( iPIn == 0 ) {
        cout << " I: ";
        cout.width(2);
        cout << vertex->nIncomingParticles();
      } else cout << "      ";
      printParticle(vertex->incomingParticle(iPIn));
    }
    for (unsigned int iPOut = 0; iPOut<vertex->nOutgoingParticles(); ++iPOut) {
      if ( iPOut == 0 ) {
        cout << " O: ";
        cout.width(2);
        cout << vertex->nOutgoingParticles();
      } else cout << "      ";
      printParticle(vertex->outgoingParticle(iPOut));
    }
  
    cout.flags(f); 
  }

  void MyxAODAnalysis::printParticle(const xAOD::TruthParticle* particle) {
    std::ios::fmtflags f( cout.flags() ); 
    cout << " ";
    cout.width(9);
    cout << particle->barcode();
    cout.width(9);
    cout << particle->pdgId() << " ";
    cout.width(9);
    cout.precision(2);
    cout.setf(ios::scientific, ios::floatfield);
    cout.setf(ios_base::showpos);
    cout << particle->px() << ",";
    cout.width(9);
    cout.precision(2);
    cout << particle->py() << ",";
    cout.width(9);
    cout.precision(2);
    cout << particle->pz() << ",";
    cout.width(9);
    cout.precision(2);
    cout << particle->e() << " ";
    cout.setf(ios::fmtflags(0), ios::floatfield);
    cout.unsetf(ios_base::showpos);
    if ( particle->hasDecayVtx() ) {
      if ( particle->decayVtx()->barcode()!=0 ) {
        cout.width(3);
        cout << particle->status() << " ";
        cout.width(9);
        cout << particle->decayVtx()->barcode();
      }
    } else {
      cout.width(3);
      cout << particle->status();
    }
    cout << endl;
    cout.flags(f); 
  }


namespace Common {
   Bool_t comparePt(const xAOD::IParticle *a, const xAOD::IParticle *b)
   {
      return CxxUtils::fpcompare::greater(a->pt(), b->pt());
   }

  Float_t DeltaPhi(float phi1, float phi2) {
      Float_t deltaPhi = fabs(phi1 - phi2);
      if (deltaPhi > TMath::Pi()) deltaPhi = 2.*TMath::Pi() - deltaPhi;
      return deltaPhi;
   }

   Float_t deltaR(const xAOD::IParticle* part1, const xAOD::IParticle* part2) {
      float deltaEta = part1->eta() - part2->eta();
      float deltaPhi = DeltaPhi(part1->phi(), part2->phi());
      float deltaR = TMath::Sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
      return deltaR;
   }

   Float_t getInvariantMass(float p1_pt, float p1_eta, float p1_phi, float p1_m, float p2_pt, float p2_eta, float p2_phi, float p2_m)
   {
      TLorentzVector p1; p1.SetPtEtaPhiM(p1_pt, p1_eta, p1_phi, p1_m);
      TLorentzVector p2; p2.SetPtEtaPhiM(p2_pt, p2_eta, p2_phi, p2_m);
      TLorentzVector v = p1 + p2;
      return v.M();
   }

   Float_t getInvariantMass(vector<const xAOD::TruthParticle*> particles) {
      if (particles.size() == 2) {
         return getInvariantMass(particles.at(0)->pt(),particles.at(0)->eta(),particles.at(0)->phi(),particles.at(0)->m(),particles.at(1)->pt(),particles.at(1)->eta(),particles.at(1)->phi(),particles.at(1)->m());
      } else {
         return -999;
      }
   }

   Bool_t passOR(const xAOD::Jet* jet , vector<const xAOD::TruthParticle*> particles) {
     for (auto particle : particles) {
       float dR = deltaR(jet, particle);
       if (dR < 0.4) return false;
     }
     return true;
   }

   string getCMDoutput(const std::string& mStr) {
      string result, file;
      FILE* pipe{popen(mStr.c_str(), "r")};
      char buffer[256];

      while(fgets(buffer, sizeof(buffer), pipe) != NULL)
      {
          file = buffer;
          result += file.substr(0, file.size() - 1);
      }

      pclose(pipe);
      return result;
  }

   double getALPsCrossSection(int runNumber, string dirSuffix = "") {
      string file_path = "/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/generation_" + to_string(runNumber) + dirSuffix + "/log.generate";
      string xSection_query = "awk '/-------  PYTHIA Event and Cross Section Statistics/{getline; getline; getline; getline; getline; getline; getline; print $13}' " + file_path;
      long double xSection = stod(getCMDoutput(xSection_query)) * pow(10,12);
      return xSection;
  }

  double getALPsFilterEff(int runNumber, string dirSuffix = "") {
      string file_path = "/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/generation_" + to_string(runNumber) + dirSuffix + "/log.generate";
      string filterEff_query = "awk '/Weighted Filter/{print $8}' " + file_path;
      double filterEff = stod(getCMDoutput(filterEff_query));
      return filterEff;
  }

}

#endif
