#include <AsgTools/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>


MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.

  // declareProperty("ElectronPtCut", m_electronPtCut = 25000.0, "Minimum electron pT (in MeV)");
  declareProperty("DAODType",m_DAOD_type = 2, "DAOD Type");
  declareProperty("print_event",print_event = false, "Print event");
  declareProperty("maxEvents",maxEvents = -1, "Max events");
  declareProperty("doPDFUncertainty",doPDFUncertainty = false, "Do PDF Uncertainty study");
  declareProperty("run_number", run_number = 0,"Run number");
  declareProperty("dir_tail",dir_tail = "","Directory suffix");
  declareProperty("submission_dir",submission_dir = "","Submission dir");
  declareProperty("PDFSet",PDFSet = "NNPDF23_lo_as_0130_qed", "PDFSet");

}

StatusCode MyxAODAnalysis :: initialize ()
{
  ANA_CHECK(book (TTree("CollectionTree", "My analysis ntuple")));
  TTree * mytree = tree("CollectionTree");
  mytree->Branch("RunNumber",&m_runNumber);
  mytree->Branch("EventNumber",&m_eventNumber);
  m_jetPt = new vector<float>();
  mytree->Branch ("jet_pt", &m_jetPt);
  m_jetPtx = new vector<float>();
  mytree->Branch ("jet_ptx", &m_jetPtx);
  m_jetPty = new vector<float>();
  mytree->Branch ("jet_pty", &m_jetPty);
  m_jetPtz = new vector<float>();
  mytree->Branch ("jet_ptz", &m_jetPtz);
  m_jetPhi = new vector<float>();
  mytree->Branch ("jet_phi", &m_jetPhi);
  m_jetEta = new vector<float>();
  mytree->Branch ("jet_eta", &m_jetEta);
  m_jetE = new vector<float>();
  mytree->Branch ("jet_e", &m_jetE);
  m_axPt = new vector<float>();
  mytree->Branch ("ax_pt", &m_axPt);
  m_axPtx = new vector<float>();
  mytree->Branch ("ax_ptx", &m_axPtx);
  m_axPty = new vector<float>();
  mytree->Branch ("ax_pty", &m_axPty);
  m_axPtz = new vector<float>();
  mytree->Branch ("ax_ptz", &m_axPtz);
  m_axPhi = new vector<float>();
  mytree->Branch ("ax_phi", &m_axPhi);
  m_axEta = new vector<float>();
  mytree->Branch ("ax_eta", &m_axEta);
  m_axE = new vector<float>();
  mytree->Branch ("ax_e", &m_axE);
  m_axStatus = new vector<float>();
  mytree->Branch ("ax_status", &m_axStatus);
  m_photonPt = new vector<float>();
  mytree->Branch ("ph_pt", &m_photonPt);
  m_photonEta = new vector<float>();
  mytree->Branch ("ph_eta", &m_photonEta);
  m_photonPhi = new vector<float>();
  mytree->Branch ("ph_phi", &m_photonPhi);
  m_photonZVtx = new vector<float>();
  mytree->Branch ("ph_zvtx", &m_photonZVtx);
  m_vax_x = new vector<float>();
  mytree->Branch ("vax_x", &m_vax_x);
  m_vax_y = new vector<float>();
  mytree->Branch ("vax_y", &m_vax_y);
  m_vax_z = new vector<float>();
  mytree->Branch ("vax_z", &m_vax_z);
  met_mpx = new vector<float>();
  mytree->Branch ("met_mpx", &met_mpx);
  met_mpy = new vector<float>();
  mytree->Branch ("met_mpy", &met_mpy);
  met = new vector<float>();
  mytree->Branch ("met", &met);
  met_phi = new vector<float>();
  mytree->Branch ("met_phi", &met_phi);
  sumet = new vector<float>();
  mytree->Branch ("sumet", &sumet);
  mytree->Branch ("deltaR_jet_ax", &deltaR_jet_ax);
  mytree->Branch ("deltaR_jet_met", &deltaR_jet_met);
  mytree->Branch ("HT", &HT);
  mytree->Branch ("sHT", &sHT);
  mytree->Branch ("HT_mpx", &HT_mpx);
  mytree->Branch ("HT_mpy", &HT_mpy);
  mytree->Branch ("HT_phi", &HT_phi);
  mytree->Branch ("n_el", &n_electrons);
  mytree->Branch ("n_mu", &n_muons);
  mytree->Branch ("n_ph", &n_photons);
  mytree->Branch ("n_jet", &n_jet);
  mytree->Branch ("n_ax", &n_ax);
  mytree->Branch ("n_vertex", &n_vertex);
  n_outgoing = new vector<int>();
  mytree->Branch ("n_outgoing", &n_outgoing);
  mytree->Branch ("s", &s);
  mytree->Branch ("deltaPhi_jet_met", &deltaPhi_jet_met);
  mytree->Branch ("deltaPhi_dijet_met", &deltaPhi_dijet_met);
  mytree->Branch ("dPhiDiCentralJets", &dPhiDiCentralJets);
  mytree->Branch ("dRDijet", &dRDijet);
  mytree->Branch ("DijetMass", &DijetMass);
  mytree->Branch ("deltaPhi_HT_met", &deltaPhi_HT_met);
  mytree->Branch ("hv_x", &hv_x);
  mytree->Branch ("hv_y", &hv_y);
  mytree->Branch ("hv_z", &hv_z);
  mytree->Branch ("mT_boson_decay", &mT_boson_decay);
  mytree->Branch ("mT_H_decay", &mT_H_decay);
  mytree->Branch ("mcweight", &mcweight);
  mytree->Branch ("weight", &weight);
  pdf_weights = new vector<Double_t>();
  mytree->Branch("pdf_weights",&pdf_weights);
  // mytree->Branch ("sumOfWeights", &sumOfWeights);

  sample_name = wk()->metaData()->castString(SH::MetaFields::sampleName);

  pdf_set = new LHAPDF::PDFSet(PDFSet);
  pdf_set->mkPDFs(pdf_sets);
  if (doPDFUncertainty) {

    // xSection = Common::getALPsCrossSection(run_number, dir_tail);
    // filterEff = Common::getALPsFilterEff(run_number, dir_tail);
    xSection = 7162.81;
    filterEff = 1;

  }

  processed_events = 0;

  m_weightTool.setTypeAndName("PMGTools::PMGTruthWeightTool/PMGTruthWeightTool");
  ANA_CHECK(m_weightTool.retrieve());

  ATH_MSG_INFO("---------Printint out weights------------");

  for (auto weight : m_weightTool->getWeightNames()) {
    cout <<  string("weight") + string(":") << weight << endl;
  }
  ATH_MSG_INFO("------------------------------------------");

  return StatusCode::SUCCESS;
}


StatusCode MyxAODAnalysis :: execute ()
{

  if (maxEvents != -1 && processed_events >= maxEvents) return StatusCode::SUCCESS;

  // retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  // xAOD::TEvent *tevent = wk()->xaodEvent();

  mcweight = eventInfo->mcEventWeight();
  weight = 1;


  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  m_jetPt->clear();
  m_jetPtx->clear();
  m_jetPty->clear();
  m_jetPtz->clear();
  m_jetEta->clear();
  m_jetPhi->clear();
  m_jetE->clear();
  m_axPt->clear();
  m_axPtx->clear();
  m_axPty->clear();
  m_axPtz->clear();
  m_photonPt->clear();
  m_photonEta->clear();
  m_photonPhi->clear();
  m_photonZVtx->clear();
  m_axEta->clear();
  m_axPhi->clear();
  m_axE->clear();
  m_vax_x->clear();
  m_vax_y->clear();
  m_vax_z->clear();
  met_mpx->clear();
  met_mpy->clear();
  met->clear();
  met_phi->clear();
  sumet->clear();
  n_outgoing->clear();
  pdf_weights->clear();

  ////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if (print_event) {
    const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
    ANA_CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents"));
 
    ATH_MSG_INFO("Number of signal events in this Athena event: " << xTruthEventContainer->size());
 
    // Signal process loop
    ATH_MSG_INFO("Printing signal event...");
    for (const xAOD::TruthEvent* evt : *xTruthEventContainer) {
      cout << endl << endl;

       // Print hard-scattering info
      const xAOD::TruthVertex* vtx = evt->signalProcessVertex();
      ATH_MSG_INFO("Signal process vertex: " << vtx);
      if (vtx)
        ATH_MSG_INFO("Position = (" << vtx->x() << ", " << vtx->y() << ", " << vtx->z() << ")");
      else
        ATH_MSG_INFO("Position n.a.");
      // Print the event particle/vtx contents
      printEvent(evt);
     }
  }

  // cout << "A weight is " << m_weightTool->getWeight("Var3cUp") << endl;

  vector<const xAOD::TruthParticle*> *axions = new vector<const xAOD::TruthParticle*>();

  bool doMonophoton = (run_number >= 500000 && run_number < 600000) || run_number == 312409;

  /////////////////
  ///  MET  //////
  ////////////////
  const xAOD::MissingETContainer *METContainer = nullptr;
  ANA_CHECK(evtStore()->retrieve(METContainer, "MET_Truth"));
  for (int i = 0 ; i < 4 ; i++){
    auto current_met = METContainer->at(i);
    met_mpx->push_back(current_met->mpx());
    met_mpy->push_back(current_met->mpy());
    met->push_back(current_met->met());
    sumet->push_back(current_met->sumet());
    met_phi->push_back(current_met->phi());
  }

  TLorentzVector metVec_tst;
  metVec_tst.SetPxPyPzE(met_mpx->at(1), met_mpy->at(1), 0, met->at(1));

  //////////////////////////////////
  ///  TRUTH PARTICLES ////////////
  /////////////////////////////////

  double pt_preselection_muon = 7000; // Monojet
  double eta_preselection_muon = 2.5; // Monojet
  if (doMonophoton) pt_preselection_muon = 6000; // Monophoton
  if (doMonophoton) eta_preselection_muon = 2.7; // Monophoton
  double eta_preselection_photon = 2.37;
  eta_preselection_photon = 4.5;

  vector<const xAOD::TruthParticle*> *electrons = new vector<const xAOD::TruthParticle*>();
  vector<const xAOD::TruthParticle*> *muons = new vector<const xAOD::TruthParticle*>();
  vector<const xAOD::TruthParticle*> *photons = new vector<const xAOD::TruthParticle*>();

  if (m_DAOD_type == 2 || m_DAOD_type == 1) {
    const xAOD::TruthParticleContainer * tParticles = nullptr;
    ANA_CHECK(evtStore()->retrieve(tParticles, "TruthParticles"));
    for (auto tParticle : *tParticles) {
      if (fabs(tParticle->pdgId()) == 51) axions->push_back(tParticle);
      if (fabs(tParticle->pdgId()) == 11) {
        if (tParticle->pt() > 7000 && fabs(tParticle->eta()) < 2.47) {
          electrons->push_back(tParticle);
        }
      }
      if (fabs(tParticle->pdgId()) == 13) {
        if (tParticle->pt() > pt_preselection_muon && fabs(tParticle->eta()) < eta_preselection_muon) {
          muons->push_back(tParticle);
        }
      }
      if (fabs(tParticle->pdgId()) == 22) {
        if (tParticle->pt() > 10000 && fabs(tParticle->eta()) < eta_preselection_photon) {
          photons->push_back(tParticle);
        }
      }

    }
    
    n_electrons = electrons->size();
    n_muons = muons->size();
    n_photons = photons->size();

    const xAOD::TruthParticleContainer * tParticlesHard = nullptr;
    if (m_DAOD_type == 2) ANA_CHECK(evtStore()->retrieve(tParticlesHard, "TruthHardParticles"));

    sort(photons->begin(),photons->end(),Common::comparePt);
    for (auto photon : *photons){
      m_photonPt->push_back(photon->pt());
      m_photonEta->push_back(photon->eta());
      m_photonPhi->push_back(photon->phi());
      if (photon->hasProdVtx()) {
        m_photonZVtx->push_back(photon->prodVtx()->z());
      } else {
        for (auto tParticle : *tParticlesHard) {
          if (fabs(tParticle->pdgId()) == 22 && tParticle->pt() == photon->pt()) {
            if (tParticle->pt() > 10000 && fabs(tParticle->eta()) < eta_preselection_photon) {
              if (tParticle->hasProdVtx()) m_photonZVtx->push_back(tParticle->prodVtx()->z());
              else m_photonZVtx->push_back(-9999);
            }
          }
        }
      }
    }

  }

  n_ax = axions->size();
  sort(axions->begin(),axions->end(),Common::comparePt);
  for (auto axion : *axions){
    m_axStatus->push_back(axion->status());
    m_axPt->push_back(axion->pt());
    m_axPtx->push_back(axion->px());
    m_axPty->push_back(axion->py());
    m_axPtz->push_back(axion->pz());
    m_axEta->push_back(axion->eta());
    m_axPhi->push_back(axion->phi());
    m_axE->push_back(axion->e());
  }

  

  /////////////////
  ///  JETS //////
  ////////////////

  double eta_preselection_jet = 2.8; // Monojet
  if (doMonophoton) eta_preselection_jet = 4.5; // Monophoton

  const xAOD::JetContainer * cJets = nullptr;
  if (m_DAOD_type == 1) {ANA_CHECK(evtStore()->retrieve(cJets, "AntiKt4TruthWZJets"));}
  else {ANA_CHECK(evtStore()->retrieve(cJets, "AntiKt4TruthDressedWZJets"));}

  vector<const xAOD::Jet*> *jets = new vector<const xAOD::Jet*>();
  for (auto jet : *cJets) {
    if (!Common::passOR(jet, *electrons)) continue;
    if (!Common::passOR(jet, *muons)) continue;
    jets->push_back(jet);
  }
  sort(jets->begin(),jets->end(),Common::comparePt);
  
  n_jet = 0;
  HT_mpx = 0;
  HT_mpy = 0;
  sHT = 0;
  deltaPhi_HT_met = -9999;
  deltaPhi_jet_met = -9999;
  deltaPhi_dijet_met = -9999;
  dPhiDiCentralJets =DijetMass -9999;
  dRDijet = -9999;
  DijetMass = -9999;
  for (auto jet : *jets) {

    if (fabs(jet->eta()) < eta_preselection_jet && jet->pt() > 30000) {
      m_jetPt->push_back(jet->pt());
      m_jetPtx->push_back(jet->px());
      m_jetPty->push_back(jet->py());
      m_jetPtz->push_back(jet->pz());

      m_jetPhi->push_back(jet->phi());
      m_jetEta->push_back(jet->eta());
      m_jetE->push_back(jet->e());
      n_jet++;

      sHT += jet->pt();
      HT_mpx += jet->pt() * TMath::Cos(jet->phi());
      HT_mpy += jet->pt() * TMath::Sin(jet->phi());
    }
  }
  if (jets->size() > 0) {
    HT = sqrt(HT_mpx*HT_mpx + HT_mpy*HT_mpy);
    HT_phi = TMath::ATan2(HT_mpy, HT_mpx);
    deltaPhi_HT_met = Common::DeltaPhi(HT_phi , met_phi->at(1));
    if (m_jetPhi->size() > 0) deltaPhi_jet_met = Common::DeltaPhi(m_jetPhi->at(0) , met_phi->at(1));
  } else {
    HT = -999;
    HT_phi = -999;
  }
  if (jets->size() >= 2) {
    TLorentzVector * p_jet1 = new TLorentzVector();
    TLorentzVector * p_jet2 = new TLorentzVector();
    p_jet1->SetPtEtaPhiE(jets->at(0)->pt(),jets->at(0)->eta(),jets->at(0)->phi(),jets->at(0)->e());
    p_jet2->SetPtEtaPhiE(jets->at(1)->pt(),jets->at(1)->eta(),jets->at(1)->phi(),jets->at(1)->e());
    deltaPhi_dijet_met = fabs((*p_jet1 + *p_jet2).DeltaPhi(metVec_tst));
    dPhiDiCentralJets = fabs(p_jet1->DeltaPhi(*p_jet2));
    dRDijet = p_jet1->DeltaR(*p_jet2);
    DijetMass = (*p_jet1 + *p_jet2).M();
  }

  ////////////////////////
  ///  TRUTH EVENTS //////
  ////////////////////////

  if (m_DAOD_type == 3){
    const xAOD::TruthParticleContainer * truthBMS = 0;
    ANA_CHECK(evtStore()->retrieve(truthBMS, "TruthBSM"));
    for (auto particle : *truthBMS) {
      if (particle->pdgId() == 51) {
        axions->push_back(particle);
        if(particle->hasProdVtx()) cout << "HAS PROD VTX!!" << endl;
      }
    }
  }

  //////////////////////////////////
  ///  TRUTH Vertices ////////////
  /////////////////////////////////

  mT_boson_decay = 0;
  mT_H_decay = 0;
  if (m_DAOD_type == 2) {
    const xAOD::TruthVertexContainer *tVertices = nullptr;
    ANA_CHECK(evtStore()->retrieve(tVertices, "TruthHardVertices"));
    n_vertex = tVertices->size();
    for (auto vertex : *tVertices) {
      n_outgoing->push_back(vertex->nOutgoingParticles());
      for (unsigned int iPOut = 0; iPOut<vertex->nOutgoingParticles(); ++iPOut) {
        const xAOD::TruthParticle * particle = vertex->outgoingParticle(iPOut);
        if (fabs(particle->pdgId()) == 23 || fabs(particle->pdgId()) == 24 || fabs(particle->pdgId()) == 25) {
          if (particle->hasDecayVtx()) {
            const xAOD::TruthVertex * bosonVertex = particle->decayVtx();
            vector<const xAOD::TruthParticle*> bosonDecayParticles;
            for (unsigned int indexOutBoson = 0; indexOutBoson < bosonVertex->nOutgoingParticles(); indexOutBoson++) {
              const xAOD::TruthParticle * bosonDecayParticle = bosonVertex->outgoingParticle(indexOutBoson);
              bosonDecayParticles.push_back(bosonDecayParticle);
            }
            if (fabs(particle->pdgId()) == 25) {
              mT_H_decay = Common::getInvariantMass(bosonDecayParticles);
            } else {
              mT_boson_decay = Common::getInvariantMass(bosonDecayParticles);
            }
          }
        }
        if (particle->pdgId() == 51) {
          hv_x = vertex->x();
          hv_y = vertex->y();
          hv_z = vertex->z();
        }
      }
    }
  }
  


  //////////////////////////////////
  ///  Delta R Jet-Axion  //////////
  /////////////////////////////////

  deltaR_jet_ax = -9999;
  if (jets->size() > 0 && axions->size() > 0) deltaR_jet_ax = Common::deltaR(jets->at(0), axions->at(0));

  //////////////////////////////////
  ///  Mandelstam s       //////////
  /////////////////////////////////
  s = 0;
  if (jets->size() > 0 && axions->size() > 0){

    // if (axions->at(0)->hasProdVtx()) {
    //   ATH_MSG_INFO("HAS PROD VTX");
    //   const xAOD::TruthVertex * vtx = axions->at(0)->prodVtx();
    //   ATH_MSG_INFO("Number incoming: " << vtx->nIncomingParticles());
    // }

    TLorentzVector * p_jet = new TLorentzVector();
    TLorentzVector * p_ax = new TLorentzVector();
    p_jet->SetPtEtaPhiE(jets->at(0)->pt(),jets->at(0)->eta(),jets->at(0)->phi(),jets->at(0)->e());
    p_ax->SetPtEtaPhiE(axions->at(0)->pt(),axions->at(0)->eta(),axions->at(0)->phi(),axions->at(0)->e());
    s = (*p_jet + *p_ax).M2();

    const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
    ANA_CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents"));
    for (const xAOD::TruthEvent* evt : *xTruthEventContainer) {
      xAOD::TruthEvent::PdfInfo pdfi = evt->pdfInfo();
      if (pdfi.valid()) {
        s = pdfi.x1*pdfi.x2*6500000*6500000;   
      }
    }
  }


  //////////////////////////////////
  ///  PDF Uncertainty    //////////
  /////////////////////////////////
  const xAOD::TruthEventContainer* xTruthEventContainer = NULL;
  ANA_CHECK( evtStore()->retrieve( xTruthEventContainer, "TruthEvents"));

  xAOD::TruthEventContainer::const_iterator truthE_itr = xTruthEventContainer->begin();
  xAOD::TruthEvent::PdfInfo pdfi = (*truthE_itr)->pdfInfo();
  if (pdfi.valid()) {
      for (size_t imem = 0; imem <= pdf_set->size()-1; imem++) {
        pdf_weights->push_back(LHAPDF::weightxxQ2( pdfi.pdgId1, pdfi.pdgId2, pdfi.x1, pdfi.x2, pdfi.Q, pdf_sets[0], pdf_sets[imem] )); 
      }
  }


  if (doPDFUncertainty) {
    Double_t combination_renorm = 1;
    if (m_runNumber == 311940) {combination_renorm = 1198.14; if (m_axPt->at(0) > 260000) combination_renorm = 0;}
    if (m_runNumber == 311941) {combination_renorm = 0.000192677; if (m_axPt->at(0) < 260000 || m_axPt->at(0) > 410000) combination_renorm = 0;}
    if (m_runNumber == 311942) {combination_renorm = 0.0222009; if (m_axPt->at(0) < 410000 || m_axPt->at(0) > 1025000) combination_renorm = 0;}
    if (m_runNumber == 311943) {combination_renorm = 0.0619222; if (m_axPt->at(0) < 1025000) combination_renorm = 0;}
    std::vector<Double_t> weights = *(new vector<Double_t>(pdf_sets.size()));
    if (weighted_yields[0].size() == 0) for (int met_bin = 0 ; met_bin < 13 ; met_bin++) weighted_yields[met_bin] = *(new vector<Double_t>(pdf_sets.size()));
    if (weighted_events.size() == 0) weighted_events = *(new vector<Double_t>(pdf_sets.size()));

    for (const xAOD::TruthEvent* evt : *xTruthEventContainer) {
      xAOD::TruthEvent::PdfInfo pdfi = evt->pdfInfo();
      if (pdfi.valid()) {
        // cout << "PDF info: PIDs " << pdfi.pdgId1 << ", " << pdfi.pdgId2 << " with x = "
        //                     << pdfi.x1 << ", " << pdfi.x2 << " & Q = " << pdfi.Q << " => xf = "
        //                     << pdfi.xf1 << ", " << pdfi.xf2 << " with PDFs "
        //                     << pdfi.pdfId1 << " and " << pdfi.pdfId2 << endl;
        for (size_t imem = 0; imem <= pdf_set->size()-1; imem++) {
          weights.at(imem) = mcweight * combination_renorm * LHAPDF::weightxxQ2( pdfi.pdgId1, pdfi.pdgId2, pdfi.x1, pdfi.x2, pdfi.Q, pdf_sets[0], pdf_sets[imem] ); 
          weighted_events.at(imem) += weights.at(imem); 
        }
      }
    }

    if (n_jet > 0 && n_jet < 5) {
        if (n_electrons == 0 && n_muons == 0) {
            if (m_jetPt->at(0) > 150000 && fabs(m_jetEta->at(0)) < 2.4 ){
              bool isDphiok = true;
              for (int i = 0 ; i < n_jet ; i++) {
                  float dphi = TMath::Abs(TMath::ACos(TMath::Cos(m_jetPhi->at(i) - met_phi->at(1))));
                  if (dphi <= 0.4) isDphiok = false;
              }
              if (isDphiok) {
                if (met->at(1) > 250000) yield++;
                
                for (const xAOD::TruthEvent* evt : *xTruthEventContainer) {
                  xAOD::TruthEvent::PdfInfo pdfi = evt->pdfInfo();
                  if (pdfi.valid()) {

                    for (size_t imem = 0; imem <= pdf_set->size()-1; imem++) {
                      // one event weight for each error in the set including the nominal
                      double met_GeV = met->at(1) / 1000.;

                      if (200. < met_GeV && met_GeV < 250.) weighted_yields[0].at(imem) += weights.at(imem);
                      if (250. < met_GeV && met_GeV < 300.) weighted_yields[1].at(imem) += weights.at(imem); 
                      if (300. < met_GeV && met_GeV < 350.) weighted_yields[2].at(imem) += weights.at(imem);
                      if (350. < met_GeV && met_GeV < 400.) weighted_yields[3].at(imem) += weights.at(imem);
                      if (400. < met_GeV && met_GeV < 500.) weighted_yields[4].at(imem) += weights.at(imem);
                      if (500. < met_GeV && met_GeV < 600.) weighted_yields[5].at(imem) += weights.at(imem);
                      if (600. < met_GeV && met_GeV < 700.) weighted_yields[6].at(imem) += weights.at(imem);
                      if (700. < met_GeV && met_GeV < 800.) weighted_yields[7].at(imem) += weights.at(imem);
                      if (800. < met_GeV && met_GeV < 900.) weighted_yields[8].at(imem) += weights.at(imem);
                      if (900. < met_GeV && met_GeV < 1000.) weighted_yields[9].at(imem) += weights.at(imem);
                      if (1000. < met_GeV && met_GeV < 1100.) weighted_yields[10].at(imem) += weights.at(imem);
                      if (1100. < met_GeV && met_GeV < 1200.) weighted_yields[11].at(imem) += weights.at(imem);
                      if (1200. < met_GeV) weighted_yields[12].at(imem) += weights.at(imem); 

                    }
                  }
                }
              }
            }
        }
    }

  }

  /////////////////////////////////

  processed_events += 1;

  tree("CollectionTree")->Fill();

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  ATH_MSG_INFO("IN FINALIZE FUNCTION");
  
  if (doPDFUncertainty) {
    cout << "==============================" << endl;
    cout << "The yield was " << yield << endl;
    cout << "The yield was " << weighted_yields[0].at(0) << endl;
    cout << "The N was " << weighted_events.at(0) << endl;
    cout << "Nominal vis_xSec = " << weighted_yields[0].at(0) * xSection * filterEff / weighted_events.at(0) << endl;
    cout << "==============================" << endl;
    TH1D * hist_PDF_uncert [13];
    vector<double> v_sigma [13];
    vector<double> v_pdfSet [13];
    double O_0 [13];
    for (int met_bin = 0 ; met_bin < 13 ; met_bin++) {
      O_0[met_bin] = weighted_yields[met_bin].at(0) * xSection * filterEff / weighted_events.at(0);
      TString histName = "hist_bin" + to_string(met_bin);
      hist_PDF_uncert[met_bin] = new TH1D(histName,"",weighted_yields[met_bin].size(),0,weighted_yields[met_bin].size());
      for (unsigned int i = 0 ; i < weighted_yields[met_bin].size() ; i++) {
        Double_t sigma_eff = weighted_yields[met_bin].at(i) * xSection * filterEff / weighted_events.at(i);
        hist_PDF_uncert[met_bin]->SetBinContent(i+1,sigma_eff);
        v_sigma[met_bin].push_back(sigma_eff);
        v_pdfSet[met_bin].push_back(i);
      }
    }

    double sigma_O [13];
    // cout << "Sgima_O" << endl;
    for (int met_bin = 0 ; met_bin < 13 ; met_bin++) {
      // if (met_bin != 2) continue;
      for (unsigned int i = 1 ; i < v_sigma[met_bin].size() ; i++) {
        // cout << v_sigma[met_bin].at(i) << "  " << O_0[met_bin] << endl;
        sigma_O[met_bin] += (v_sigma[met_bin].at(i) - O_0[met_bin])*(v_sigma[met_bin].at(i) - O_0[met_bin]);
      }
      sigma_O[met_bin] /= (v_sigma[met_bin].size() - 1);
      sigma_O[met_bin] = sqrt(sigma_O[met_bin]);
    }

    cout << "==============================" << endl;
    cout << "The uncertainties would be: ";
    for (int met_bin = 0 ; met_bin < 13 ; met_bin++) cout << " " << 100*sigma_O[met_bin] / O_0[met_bin];
    cout << endl;
    cout << "==============================" << endl;
    cout << "sigma_O = " << sigma_O[2] << endl;
    cout << "O_0 = " << O_0[2] << endl;
    cout << "weighted_yields = " << weighted_yields[2].at(0) << endl;
    cout << "weighted_events = " << weighted_events[2] << endl;
    cout << "xSection = " << xSection << endl;
    cout << "filterEff = " << filterEff << endl;


    vector<double> v_sigmaMax, v_sigmaMin;
    for (unsigned int i = 0 ; i < v_sigma[0].size() ; i++) {
      v_sigmaMax.push_back(O_0[0] + sigma_O[0]);
      v_sigmaMin.push_back(O_0[0] - sigma_O[0]);
    }

    TGraph *grmin = new TGraph(v_sigma[0].size(),&v_pdfSet[0].at(0),&v_sigmaMin.at(0));
    TGraph *grmax = new TGraph(v_sigma[0].size(),&v_pdfSet[0].at(0),&v_sigmaMax.at(0));
    TGraph *gr = new TGraph(v_sigma[0].size(),&v_pdfSet[0].at(0),&v_sigma[0].at(0));

    TGraph * grshade = new TGraph(2*v_sigma[0].size());
    for (unsigned int i = 0 ; i < v_sigma[0].size() ; i++) {
      grshade->SetPoint(i,v_pdfSet[0].at(i),v_sigmaMax.at(i));
      grshade->SetPoint(v_sigma[0].size()+i,v_pdfSet[0].at(v_sigma[0].size()-i-1),v_sigmaMin.at(v_sigma[0].size()-i-1));
    }

    TString fileName = "/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/analysis/run/" + submission_dir + "/PDFUncertanty.root";
    TFile * file = new TFile(fileName,"RECREATE");
    file->cd();
    hist_PDF_uncert[0]->Write();
    grmin->Write("grmin");
    grmax->Write("grmax");
    gr->Write("gr");
    grshade->Write("grshade");

  }


  return StatusCode::SUCCESS;
}
