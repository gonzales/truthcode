#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option('-l', '--inputList', type=str, dest='txt', help='comma-separated list of txt files, one per sample, containing file paths', 
                    metavar='list', default='')  
parser.add_option( '-i', '--input-type', dest = 'input_type',
                   action = 'store', type = 'int', default = 1,
                   help = 'Reduction configuration TRUTH<i>' )
parser.add_option( '-r', '--run-number', dest = 'run_number',
                   action = 'store', type = 'int', default = -1,
                   help = 'Sample run number' )
parser.add_option( '-t', '--dir-tail', dest = 'dir_tail',
                    action = 'store', type = 'string', default = '',
                    help = 'String to add to the directory where the DAOD is located at' )
parser.add_option('-p','--print',dest='print_event', action='store_true', default=False)
parser.add_option('-m','--maxEvents',dest='maxEvents', action = 'store', type = 'int', default=-1)
parser.add_option('-a','--doPDFUncertainty',dest='doPDFUncertainty', action = 'store_true', default=False)
parser.add_option('-f','--PDFSet',dest='PDFSet', action = 'store', default="NNPDF23_lo_as_0130_qed")
parser.add_option('-d','--dataFilePath',dest='dataFilePath', action = 'store', type = 'string', default = '', help = 'Path to the folder with the TRUTH file')
parser.add_option('-n','--fileName',dest='fileName', action = 'store', type = 'string', default = '', help = 'Name of the TRUTH file')

( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

truthType = 'TRUTH' + str(options.input_type)

if options.dataFilePath != '':
    inputFilePath = options.dataFilePath
    if options.fileName == '': ROOT.SH.ScanDir().filePattern('DAOD_TRUTH*pool.root').scan(sh, inputFilePath)
    else: ROOT.SH.ScanDir().filePattern(options.fileName).scan(sh, inputFilePath)
else:
    if options.run_number == 0:
        inputFilePath = '/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/DERIVATIONS/TRUTH3_100053' + options.dir_tail
        ROOT.SH.ScanDir().filePattern( 'DAOD_TRUTH3.DAOD_TRUTH3.ALP_100053.TRUTH3.pool.root' ).scan( sh, inputFilePath )
    elif options.run_number == 306621:
        inputFilePath = '/nfs/pic.es/user/s/sgonzalez/scratch2/rucio/mc15_13TeV.306621.aMcAtNloPy8EG_N30NLO_A14N23LO_DMsimpA_DM10_M100_gam130.merge.DAOD_TRUTH1.e5546_p2913/'
        ROOT.SH.ScanDir().filePattern( 'DAOD_TRUTH1.10372774._000001.pool.root.1' ).scan( sh, inputFilePath )
    elif options.run_number == 100000:
        inputFilePath = '/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/DERIVATIONS/' + truthType + '_1MeV_1' + options.dir_tail
        ROOT.SH.ScanDir().filePattern( 'DAOD_' + truthType + '.ALP_1MeV_1.' + truthType + '.pool.root' ).scan( sh, inputFilePath )
    elif options.run_number == 100001:
        inputFilePath = '/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/DERIVATIONS/' + truthType + '_1MeV_0025' + options.dir_tail
        ROOT.SH.ScanDir().filePattern( 'DAOD_' + truthType + '.ALP_1MeV_0025.' + truthType + '.pool.root' ).scan( sh, inputFilePath )
    elif options.run_number == -1 and options.txt != '':
        getlist = lambda x: filter(lambda y: y != '', x.replace(' ', '').split(','))
        txts = getlist(options.txt)
        for txt in txts:
            sampleName = ''.join(txt.split('/')[-1]) # /a/b/c/d.txt -> d
            print 'adding txt ',sampleName
            ROOT.SH.readFileList(sh, sampleName, txt)
        # dirs = getlist(options.txt)
        # for dir in dirs:
        #     print 'adding dir ',dir
        #     ROOT.SH.scanDir(sh, dir)
    elif options.run_number <= 100005:
        inputFilePath = '/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/DERIVATIONS/' + truthType + '_' + str(options.run_number) + options.dir_tail
        ROOT.SH.ScanDir().filePattern( 'DAOD_' + truthType + '.ALP_' + str(options.run_number) + '.' + truthType + '.pool.root' ).scan( sh, inputFilePath )
    else:
        inputFilePath = '/nfs/pic.es/user/s/sgonzalez/scratch2/ALPs/DERIVATIONS/' + truthType + '_' + str(options.run_number) + options.dir_tail
        ROOT.SH.ScanDir().filePattern('DAOD_' + truthType + '.DAOD_TRUTH2.ALP_' + str(options.run_number) + '.' + truthType + '.pool.root').scan( sh, inputFilePath )


sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
# job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )

job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )
# config.ElectronPtCut = 30000.0
# config.SampleName = 'Zee'
config.DAODType = options.input_type
config.print_event = options.print_event
config.maxEvents = options.maxEvents
config.doPDFUncertainty = options.doPDFUncertainty
config.run_number = options.run_number
config.dir_tail = options.dir_tail
config.submission_dir = "results/" + options.submission_dir
config.PDFSet = options.PDFSet
job.algsAdd( config )

os.system("mkdir -p results")

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
if (options.run_number != 0):
    driver.submit( job, "results/" + options.submission_dir )
else:
    driver.submit( job, "results/" + options.submission_dir )

